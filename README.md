# JSS Training

[Confluence Docs](https://horizontal.atlassian.net/wiki/spaces/HFEOS/pages/7273396437150/JSS)

This repo contains sample environments with the packages necessary to complete the trainings located in Confluence.

## Setup

Fork this repository.

Follow along with each chapter in Confluence.

- For the React & NextJS training sections, example code for each chapter can be found in the `examples` folder of each project.
- For the JSS training, an example of the completed code can be found on the `examples/jss-training` branch.
