// Global
import dynamic from 'next/dynamic';
import { hasDataComponent, snapshot } from 'lib/jest/test-utils';
// Components
import IconNewTab from 'components/helpers/SvgIcon/icons/icon--new-tab';
// Local
import Accordion from './Accordion';
import defaultData from './Accordion.mock-data';

// Mock out the SvgIcon dynamic import
jest.mock('next/dynamic');
      
beforeAll(() => {
  dynamic.mockImplementation(() => IconNewTab);
});

it('renders correctly', () => {
  const component = snapshot(Accordion, { componentProps: defaultData });
  hasDataComponent(component, 'authorable/general/accordion');
});
