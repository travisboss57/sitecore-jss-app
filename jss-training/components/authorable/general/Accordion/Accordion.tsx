// Global
import HeadingTag from '@dullaghan/heading-tag';
import { ComponentFields, Field, Text } from '@sitecore-jss/sitecore-jss-nextjs';
import Container from 'components/helpers/Container/Container';
import Heading from 'components/helpers/Heading/Heading';
import { Children } from 'react';
import AccordionEntry from './AccordionEntry';

interface AccordionEntry {
  id: string;
  fields?: {
    accordionEntryBody: Field<string>;
    accordionEntryTitle: Field<string>;
  };
}

export interface AccordionProps {
  fields?: {
    root: {
      children: AccordionEntry[];
      fields?: {
        headingText: Field<string>;
      };
    };
  };
}

const Accordion = ({ fields }: AccordionProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields || !fields.root.fields) {
    return <></>;
  }

  return (
    <Container dataComponent="authorable/general/accordion">
      <Heading field={fields.root.fields.headingText} className="mb-8" />
      <ul>
        {fields.root.children.map((child) => {
          if (!!child.fields) {
            return (
              <li key={child.id} className="mb-2 last:mb-0">
                <AccordionEntry
                  title={child.fields.accordionEntryTitle}
                  body={child.fields.accordionEntryBody}
                  headingLevel={3}
                />
              </li>
            );
          }
        })}
      </ul>
    </Container>
  );
};

export default Accordion;
