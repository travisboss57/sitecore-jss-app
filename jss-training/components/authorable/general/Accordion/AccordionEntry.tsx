// Global
import { Field, Text } from '@sitecore-jss/sitecore-jss-nextjs';
import { useState } from 'react';
import classNames from 'classnames';
import HeadingTag, { HeadingLevel } from '@dullaghan/heading-tag';
import RichTextWrapper from 'components/helpers/RichTextWrapper/RichTextWrapper';
import SvgIcon from 'components/helpers/SvgIcon/SvgIcon';

interface AccordionEntryProps {
  title: Field<string>;
  body: Field<string>;
  headingLevel: HeadingLevel;
}

const AccordionEntry = ({ title, body, headingLevel = 3 }: AccordionEntryProps): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="w-full">
      <button
        className="flex justify-between p-4 bg-theme-bg-alt w-full"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <HeadingTag level={headingLevel} className="max-w-prose font-black text-left">
          <Text field={title} tag="" />
        </HeadingTag>
        <span className="text-theme-btn-bg">
          <SvgIcon icon={isOpen ? 'minus' : 'plus'} />
        </span>
      </button>
      <div className={classNames('p-4', 'max-w-prose', { hidden: !isOpen })}>
        <RichTextWrapper field={body} />
      </div>
    </div>
  );
};

export default AccordionEntry;
