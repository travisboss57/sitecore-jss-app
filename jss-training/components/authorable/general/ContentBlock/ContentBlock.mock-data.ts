const defaultData = {
  fields: {
    content: {
      value:
        "Every single thing in the world has its own personality - and it is up to you to make friends with the little rascals. This is gonna be a happy little seascape. There is no right or wrong - as long as it makes you happy and doesn't hurt anyone. Let's just drop a little Evergreen right here. Look around, look at what we have. Beauty is everywhere, you only have to look to see it.&nbsp;At home you have unlimited time. Here's another little happy bush When things happen - enjoy them. They're little gifts.",
    },
    cta: {
      value: {
        href: '',
        text: null,
      },
    },
    headingScreenReaderOnly: {
      value: false,
    },
    headingText: {
      value: 'There are no mistakes',
    },
    headingLevel: {
      fields: {
        Value: {
          value: 'h2',
        },
      },
    },
    headingBranding: {
      value: false,
    },
    headingSize: {
      fields: {
        Value: {
          value: 'lg',
        },
      },
    },
  },
};

export default defaultData;
