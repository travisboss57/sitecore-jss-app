// Global
import { Story, Meta } from '@storybook/react';
// Local
import ContentBlock, { ContentBlockProps } from './ContentBlock';
import defaultData from './ContentBlock.mock-data';

export default {
  title: 'Authorable/General/ContentBlock',
  component: ContentBlock,
} as Meta;

const Template: Story<ContentBlockProps> = (props) => <ContentBlock {...props} />;

export const Default = Template.bind({});
Default.args = defaultData;
