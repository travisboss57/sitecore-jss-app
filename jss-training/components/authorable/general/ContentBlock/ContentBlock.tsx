// Components
import { Field } from '@sitecore-jss/sitecore-jss-nextjs';
import Container from 'components/helpers/Container/Container';
import Heading from 'components/helpers/Heading/Heading';
import RichTextWrapper from 'components/helpers/RichTextWrapper/RichTextWrapper';

export interface ContentBlockProps {
  fields?: {
    headingText: Field<string>;
    content: Field<string>;
  };
}

const ContentBlock = ({ fields }: ContentBlockProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) {
    return <></>;
  }

  return (
    <Container dataComponent="authorable/general/contentblock">
      <Heading field={fields.headingText} />
      <div className="max-w-prose text-lg">
        <RichTextWrapper field={fields.content} />
      </div>
    </Container>
  );
};

export default ContentBlock;
