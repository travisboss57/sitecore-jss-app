// Global
import { Story, Meta } from '@storybook/react';
// Local
import HeroStandard, { HeroStandardProps } from './HeroStandard';
import defaultData from './HeroStandard.mock-data';

export default {
  title: 'Authorable/Hero/HeroStandard',
  component: HeroStandard,
} as Meta;

const Template: Story<HeroStandardProps> = (props) => <HeroStandard {...props} />;

export const Default = Template.bind({});
Default.args = defaultData;
