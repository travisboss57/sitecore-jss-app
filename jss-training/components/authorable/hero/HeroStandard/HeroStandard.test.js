// Global
import { hasDataComponent, snapshot } from 'lib/jest/test-utils';
// Local
import HeroStandard from './HeroStandard';
import defaultData from './HeroStandard.mock-data';

it('renders correctly', () => {
  const component = snapshot(HeroStandard, { componentProps: defaultData });
  hasDataComponent(component, 'authorable/hero/herostandard');
});
