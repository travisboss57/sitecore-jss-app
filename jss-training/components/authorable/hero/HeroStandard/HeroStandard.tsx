// Global
import { Field, Text } from '@sitecore-jss/sitecore-jss-nextjs';
// Lib
import { EnumField, getEnum } from 'lib/get-enum';
// Components
import Container from 'components/helpers/Container/Container';
import Heading from 'components/helpers/Heading/Heading';
import ImageWrapper, { SizedImageField } from 'components/helpers/ImageWrapper/ImageWrapper';
import classNames from 'classnames';
import { useFirstSectionTheme } from 'lib/use-first-section-theme';

type HeroLayout = 'bottom' | 'bottom-split' | 'top';

export interface HeroStandardProps {
  fields?: {
    heroHeadingText: Field<string>;
    heroImage: SizedImageField;
    heroImagePlacement: EnumField<HeroLayout>;
    heroLabelText: Field<string>;
  };
}

const HeroStandard = ({ fields }: HeroStandardProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) {
    return <></>;
  }

  const layout = getEnum<HeroLayout>(fields.heroImagePlacement) || 'bottom';

  const firstSectionTheme = useFirstSectionTheme() || 'theme-inherit';

  return (
    <div className="relative py-10">
      <div className="relative z-20">
        <Container dataComponent="authorable/hero/herostandard">
          {layout === 'top' && (
            <div className="mb-8">
              <ImageWrapper image={fields.heroImage} />
            </div>
          )}
          <Text tag="p" field={fields.heroLabelText} className="font-black text-lg" />
          <Heading field={fields.heroHeadingText} level={1} size="lg" />
          {layout !== 'top' && (
            <div className={classNames('mt-8', { 'mb-20': layout === 'bottom' })}>
              <ImageWrapper image={fields.heroImage} />
            </div>
          )}
        </Container>
      </div>
      {layout === 'bottom-split' && (
        <div
          className={classNames(
            firstSectionTheme,
            'absolute',
            'bg-theme-bg',
            'bottom-0',
            'h-1/2',
            'inset-x-0',
            'z-10'
          )}
        />
      )}
    </div>
  );
};

export default HeroStandard;
