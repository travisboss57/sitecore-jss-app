// Global
import { ComponentRendering } from '@sitecore-jss/sitecore-jss-nextjs';
import classNames from 'classnames';
// Lib
import { EnumField, getEnum } from 'lib/get-enum';
import { Theme, getThemeClasses } from 'lib/get-theme';
// Components
import PlaceholderWrapper from 'components/helpers/PlaceholderWrapper/PlaceholderWrapper';

export interface LayoutPageSectionProps {
  fields?: {
    sectionTheme: EnumField<Theme>;
  };
  rendering: ComponentRendering;
}

const LayoutPageSection = ({ fields, rendering }: LayoutPageSectionProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) {
    return <></>;
  }

  return (
    <div
      data-component="authorable/layout/layoutpagesection"
      className={getThemeClasses(getEnum<Theme>(fields.sectionTheme || 'inherit'))}
    >
      <PlaceholderWrapper
        rendering={rendering}
        name="hztl-layout-page-section"
        render={(components) => <div className="grid gap-20">{components}</div>}
        renderEmpty={(components) => ({ components })}
      />
    </div>
  );
};

export default LayoutPageSection;
