// Lib
import { getSampleRenderingContext } from 'lib/mocks/mock-placeholder';

const defaultData = {
  rendering: {
    ...getSampleRenderingContext('hztl-layout-split-seventy-five-percent'),
    componentName: 'Split2575',
  },
  fields: {
    description: {
      value: '',
    },
    headingScreenReaderOnly: {
      value: false,
    },
    headingText: {
      value: 'Split 25-75',
    },
    headingLevel: {
      fields: {
        Value: {
          value: 'h2',
        },
      },
    },
  },
};

export default defaultData;
