// Global
import { Field, ComponentRendering } from '@sitecore-jss/sitecore-jss-nextjs';
// Components
import PlaceholderWrapper from 'components/helpers/PlaceholderWrapper/PlaceholderWrapper';
import Container from 'components/helpers/Container/Container';
import Heading from 'components/helpers/Heading/Heading';

export interface Split2575Props {
  fields?: {
    headingText: Field<string>;
  };
  rendering: ComponentRendering;
}

const Split2575 = ({ fields, rendering }: Split2575Props): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) {
    return <></>;
  }

  return (
    <Container dataComponent="authorable/layout/split2575">
      <div className="grid gap-6 md:grid-cols-4">
        <div>
          <Heading level={2} field={fields.headingText} size="sm" />
        </div>
        <div className="col-span-3">
          <PlaceholderWrapper
            rendering={rendering}
            name="hztl-layout-split-seventy-five-percent"
            render={(components) => <>{components}</>}
            renderEmpty={(components) => ({ components })}
          />
        </div>
      </div>
    </Container>
  );
};

export default Split2575;
