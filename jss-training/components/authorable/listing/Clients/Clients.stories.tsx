// Global
import { ComponentPropsContext } from '@sitecore-jss/sitecore-jss-nextjs';
import { Story, Meta } from '@storybook/react';
// Local
import Clients, { ClientsProps } from './Clients';
import defaultData, { staticPropsData } from './Clients.mock-data';

export default {
  title: 'Authorable/Listing/Clients',
  component: Clients,
  decorators: [
    (Story) => <ComponentPropsContext value={staticPropsData}>{Story()}</ComponentPropsContext>,
  ],
} as Meta;

const Template: Story<ClientsProps> = (props) => <Clients {...props} />;

export const Default = Template.bind({});
Default.args = defaultData;
