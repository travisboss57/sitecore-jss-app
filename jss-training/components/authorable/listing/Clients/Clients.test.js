// Global
import { hasDataComponent, snapshot } from 'lib/jest/test-utils';
// Local
import Clients from './Clients';
import defaultData, { staticPropsData } from './Clients.mock-data';

it('renders correctly', () => {
  const component = snapshot(Clients, { componentProps: defaultData, staticProps: staticPropsData });
  hasDataComponent(component, 'authorable/listing/clients');
});
