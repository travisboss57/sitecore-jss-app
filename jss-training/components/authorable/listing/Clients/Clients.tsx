// Global
import { useLazyQuery } from '@apollo/client';
import { useState, useEffect } from 'react';
import {
  Field,
  GetStaticComponentProps,
  useComponentProps,
  ComponentRendering,
} from '@sitecore-jss/sitecore-jss-nextjs';
import classNames from 'classnames';
// Lib
import graphQLClientFactory from 'lib/graphql/client-factory';
import { normalizeGuid } from 'lib/sitecore/normalize-guid';
import { forceRelativeUrl } from 'lib/force-relative-url';
// Components
import ImageWrapper, { SizedImageField } from 'components/helpers/ImageWrapper/ImageWrapper';
import Container from 'components/helpers/Container/Container';
import Heading from 'components/helpers/Heading/Heading';
// Local
import { GET_CLIENTS, GET_TAXONOMY } from './Clients.graphql';

export interface ClientsProps {
  fields?: {
    headingText: Field<string>;
  };
  rendering: ComponentRendering;
}

type FilterData = {
  name: string;
  id: string;
};

type ClientData = {
  id: string;
  name: string;
  image: SizedImageField;
};

type ClientGraphQLData = {
  clientName: { jsonValue: Field<string> };
  id: string;
  clientLogo: { jsonValue: SizedImageField };
};

type ClientSearchResult = {
  search: {
    results: ClientGraphQLData[];
  };
};

export interface ClientsStaticData {
  filters: FilterData[];
  results: ClientData[];
}

// We only ever want to show 12 results
const MAX_RESULTS = 12;

const getQueryPredicate = (filter?: string) => {
  const vars = {
    AND: [
      {
        name: '_templatename',
        value: 'Client',
        operator: 'EQ',
      },
    ],
  };

  if (!!filter) {
    vars.AND.push({
      name: 'businessVerticals',
      value: filter,
      operator: 'EQ',
    });
  }

  return vars;
};

const transformClientResults = (data: ClientSearchResult): ClientData[] =>
  data.search.results.map((client) => ({
    name: client.clientName.jsonValue.value,
    id: normalizeGuid(client.id) || '',
    image: {
      value: {
        src: forceRelativeUrl(client.clientLogo.jsonValue.value?.src),
        alt: client.clientLogo.jsonValue.value?.alt || '',
        height: client.clientLogo.jsonValue.value?.height || '',
        width: client.clientLogo.jsonValue.value?.width || '',
      },
    },
  }));

const Clients = ({ fields, rendering }: ClientsProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) {
    return <></>;
  }

  const staticPropsData = rendering?.uid
    ? useComponentProps<ClientsStaticData>(rendering.uid)
    : undefined;

  if (!staticPropsData) {
    return <></>;
  }

  const [activeFilter, setActiveFilter] = useState('');

  const [fetchClients, { called, loading, data }] = useLazyQuery(GET_CLIENTS);

  useEffect(() => {
    if (!!activeFilter) {
      fetchClients({ variables: { where: getQueryPredicate(activeFilter), count: MAX_RESULTS } });
    }
  }, [activeFilter]);

  const results: ClientData[] =
    !!activeFilter && !!data ? transformClientResults(data) : staticPropsData.results;

  return (
    <Container dataComponent="authorable/listing/clients">
      <div className="md:flex space-between items-end">
        <Heading field={fields.headingText} level={2} size="sm" />
        <ul className="flex md:justify-end md:flex-grow md:ml-auto">
          {staticPropsData.filters.map((filter) => (
            <li key={filter.id} className={classNames('mr-4 last:mr-0')}>
              <button
                onClick={() => {
                  setActiveFilter(filter.id === activeFilter ? '' : filter.id);
                }}
                className={classNames('font-bold', 'hover:underline', {
                  underline: filter.id === activeFilter,
                  'text-theme-text-alt': filter.id !== activeFilter,
                })}
              >
                {filter.name}
              </button>
            </li>
          ))}
        </ul>
      </div>
      <ul className="grid gap-6 grid-cols-2 md:grid-cols-4">
        {results.map((client) => (
          <li key={client.id}>
            <ImageWrapper image={client.image} editable={false} />
          </li>
        ))}
      </ul>
    </Container>
  );
};

/* istanbul ignore next - We aren't running E2E tests. */
export const getStaticProps: GetStaticComponentProps = async () => {
  const graphQLClient = graphQLClientFactory();

  const taxonomy = await graphQLClient.query({
    query: GET_TAXONOMY,
  });

  const result = await graphQLClient.query({
    query: GET_CLIENTS,
    variables: {
      where: getQueryPredicate(),
      count: MAX_RESULTS,
    },
  });

  return {
    filters: taxonomy.data.businessVerticals.children.results.map((filter: FilterData) => ({
      name: filter.name,
      id: normalizeGuid(filter.id),
    })),
    results: transformClientResults(result.data),
  };
};

export default Clients;
