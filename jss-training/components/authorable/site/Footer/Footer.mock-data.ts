const UID = 'footer-uid';
const defaultData = {
  rendering: {
    uid: UID,
componentName: 'Footer',
  },
};
  
export const staticPropsData = {
  [UID]: {},
};

export default defaultData;
