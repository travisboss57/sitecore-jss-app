// Global
import { ComponentPropsContext } from '@sitecore-jss/sitecore-jss-nextjs';
import { Story, Meta } from '@storybook/react';
// Local
import Footer, { FooterProps } from './Footer';
import defaultData, { staticPropsData } from './Footer.mock-data';

export default {
  title: 'Authorable/Site/Footer',
  component: Footer,
  decorators: [
    (Story) => <ComponentPropsContext value={staticPropsData}>{Story()}</ComponentPropsContext>,
  ],
} as Meta;

const Template: Story<FooterProps> = (props) => <Footer {...props} />;

export const Default = Template.bind({});
Default.args = defaultData;
