// Global
import { GetStaticComponentProps, useComponentProps, ComponentRendering } from '@sitecore-jss/sitecore-jss-nextjs';
// Lib
import graphQLClientFactory from 'lib/graphql/client-factory';
import query from './Footer.graphql';
// Components
import Container from 'components/helpers/Container/Container'

export interface FooterProps {
  fields?: {};
  rendering: ComponentRendering;
}
export interface FooterStaticData {
  datasource: {}
}

const Footer = ({ fields, rendering }: FooterProps): JSX.Element => {
  // Fail out if we don't have any fields
  if (!fields) { 
    return <></>;
  }
  
    const graphQlData = rendering?.uid ? useComponentProps<FooterStaticData>(rendering.uid) : undefined;
  
  return (
    <Container dataComponent="authorable/site/footer">
      Footer
    </Container>
  );
};

/* istanbul ignore next - We aren't running E2E tests. */
export const getStaticProps: GetStaticComponentProps = async (rendering, layoutData) => {
  const graphQLClient = graphQLClientFactory();
  
  const result = await graphQLClient.query({
    query,
    variables: {
      datasource: rendering.dataSource,
      contextItem: layoutData?.sitecore?.route?.itemId,
    },
  });
  
  return result.data;
};

export default Footer;
