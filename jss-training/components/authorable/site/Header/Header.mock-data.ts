const UID = 'header-uid';
const defaultData = {
  rendering: {
    uid: UID,
componentName: 'Header',
  },
};
  
export const staticPropsData = {
  [UID]: {},
};

export default defaultData;
