// Global
import { ComponentPropsContext } from '@sitecore-jss/sitecore-jss-nextjs';
import { Story, Meta } from '@storybook/react';
// Local
import Header, { HeaderProps } from './Header';
import defaultData, { staticPropsData } from './Header.mock-data';

export default {
  title: 'Authorable/Site/Header',
  component: Header,
  decorators: [
    (Story) => <ComponentPropsContext value={staticPropsData}>{Story()}</ComponentPropsContext>,
  ],
} as Meta;

const Template: Story<HeaderProps> = (props) => <Header {...props} />;

export const Default = Template.bind({});
Default.args = defaultData;
