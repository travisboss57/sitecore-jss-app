// Global
import dynamic from 'next/dynamic';
import { hasDataComponent, snapshot } from 'lib/jest/test-utils';
// Components
import IconNewTab from 'components/helpers/SvgIcon/icons/icon--new-tab';
// Local
import Header from './Header';
import defaultData, { staticPropsData } from './Header.mock-data';

// Mock out the SvgIcon dynamic import
jest.mock('next/dynamic');
      
beforeAll(() => {
  dynamic.mockImplementation(() => IconNewTab);
});

it('renders correctly', () => {
  const component = snapshot(Header, { componentProps: defaultData, staticProps: staticPropsData });
  hasDataComponent(component, 'authorable/site/header');
});
