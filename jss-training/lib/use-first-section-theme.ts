// Global
import { useSitecoreContext } from '@sitecore-jss/sitecore-jss-nextjs';
// Local
import { ExtendedSitecoreContext } from './sitecore/sitecore-context';
import { EnumField, getEnum } from './get-enum';
import { Theme } from './get-theme';

const MAIN_PLACEHOLDER = 'hztl-main';

interface PageSection {
  fields?: {
    sectionTheme: EnumField<Theme>;
  };
}

export const useFirstSectionTheme = (): Theme | undefined => {
  const context = useSitecoreContext<ExtendedSitecoreContext>();

  // Fail if we aren't in a Sitecore context
  if (!context.sitecoreContext) {
    return undefined;
  }

  const { placeholders } = context.sitecoreContext.route;

  if (placeholders.hasOwnProperty(MAIN_PLACEHOLDER) && placeholders[MAIN_PLACEHOLDER].length > 0) {
    return getEnum<Theme>((placeholders[MAIN_PLACEHOLDER][0] as PageSection).fields?.sectionTheme);
  }

  return undefined;
};
