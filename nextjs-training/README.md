# NextJS Training

[NextJS for JSS](https://horizontal.atlassian.net/wiki/spaces/HFEOS/pages/7273409904698/NextJS+for+JSS)

This is the practice space for our crash course on NextJs. If you haven't yet completed the `react-training` section, please do so before moving on to this.

## Setup

```
npm install
npm run dev
```

Open your browser to `http://localhost:3000`. If you haven't completed any chapters it will be a completely blank page. Perfect for you to fill with your brilliant learnings.
