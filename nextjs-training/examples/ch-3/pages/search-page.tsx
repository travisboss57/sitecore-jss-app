// Interfaces
import type { MockSearchResult, MockSortBy } from '../third-party/mock-api';
import type { NextPage } from 'next';
// Global
import { FormEvent, useState } from 'react';
import axios from 'axios';
// Lib
import search from '../lib/search-integraton';
// Components
import SearchResult from '../components/SearchResult';

interface SearchPageProps {
  initialHits: MockSearchResult[];
}

export const getStaticProps = async () => {
  const response = await search({ sortBy: 'date' });

  return {
    props: {
      initialHits: response.hits,
    },
  };
};

const SearchPage: NextPage<SearchPageProps> = ({ initialHits }) => {
  const [sortBy, setSortBy] = useState<MockSortBy>('date');
  const [hits, setHits] = useState<MockSearchResult[]>(initialHits);

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    axios.get(`/api/search?sortBy=${sortBy}`).then((response) => {
      setHits(response.data.hits);
    });
  };

  return (
    <div className="max-w-prose mx-auto py-12">
      {/* Controls */}
      <form
        onSubmit={handleSubmit}
        className="flex items-center justify-between"
      >
        <div>
          <label htmlFor="sort-by" className="text-sm">
            Sort By
          </label>
          <select
            name="sortBy"
            id="sort-by"
            className="border block"
            onChange={(event) => {
              setSortBy(event.target.value as MockSortBy);
            }}
          >
            <option value="date">Date</option>
            <option value="author">Author</option>
          </select>
        </div>
        <button type="submit" className="p-4 bg-blue-400 hover:bg-blue-500">
          Search
        </button>
      </form>
      {/* Results */}
      <ul className="py-4">
        {hits.map((hit) => (
          <SearchResult {...hit} key={hit.id} />
        ))}
      </ul>
    </div>
  );
};

export default SearchPage;
