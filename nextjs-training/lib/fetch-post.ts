import fs from 'fs';
import path from 'path';

export interface PostData {
  title: string;
  description: string;
  date: number;
  copy: string[];
}

// This mock function isn't actually asynchronous, but we're pretending it is
// for the sake of our example
const fetchPost = async (post: string): Promise<PostData | null> => {
  const SAMPLE_POST_PATH = path.join(
    __dirname,
    '../../../../public/sample-posts'
  );
  const data = fs.readFileSync(`${SAMPLE_POST_PATH}/${post}.json`, {
    encoding: 'utf-8',
  });
  if (!!data) {
    return Promise.resolve(JSON.parse(data));
  }
  return Promise.resolve(null);
};

export default fetchPost;
