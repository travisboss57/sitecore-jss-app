// Global
import { useContext } from 'react';
// Local
import { MyFormContext } from './MyForm';

const MyInput = ({ name, label }) => {
  const { values, setValues } = useContext(MyFormContext);

  const handleChange = (event) => {
    const newValues = { ...values, [name]: event.target.value };
    setValues(newValues);
  };

  return (
    <div>
      <label>{label}</label>
      <input name={name} value={values[name]} onChange={handleChange} />
    </div>
  );
};

export default MyInput;
